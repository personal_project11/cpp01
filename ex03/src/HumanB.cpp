
#include "../inc/HumanB.hpp"

HumanB::HumanB(std::string name) :
	_name(name), _w(NULL)
{
	
}

void HumanB::attack() const
{
	if(_w)
	std::cout<<this->_name<<" attacks with their"
		<<this->_w->getType()<<std::endl;
	else
		std::cout<<this->_name<<" has no weapon to attack!"<<std::endl;
}

void HumanB::setWeapon(Weapon &w) 
{
	this->_w = &w;	
}
