#include "../inc/HumanA.hpp"

HumanA::HumanA(std::string name, Weapon& w) :
	 _name(name), _w(w)
{
}

void HumanA::attack() const
{
	std::cout<<this->_name<<" attacks with their "
		<<this->_w.getType()<<std::endl;
}
