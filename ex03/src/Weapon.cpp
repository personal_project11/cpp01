#include "../inc/Weapon.hpp"

Weapon::Weapon(std::string type)
{
	this->_type = type;
}

Weapon::Weapon()
{
	this->_type ="";
}
const std::string& Weapon::getType() const
{
	return _type;
}

void Weapon::setType(std::string type)
{
	this->_type = type;
}
