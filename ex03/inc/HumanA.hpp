
#ifndef HUMAN_A_HPP
#define HUMAN_A_HPP
#include <string>
#include <iostream>
#include "Weapon.hpp"

class HumanA
{
	private:
		std::string _name;
		Weapon &_w;	
	public:
		HumanA(std::string name, Weapon& w);
		void attack() const;
};

#endif
