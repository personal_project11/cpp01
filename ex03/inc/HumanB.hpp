
#ifndef HUMAN_B_HPP
#define HUMAN_B_HPP
#include <iostream>
#include <string>
#include "./Weapon.hpp"

class HumanB
{
	private:
		std::string _name;
		Weapon *_w;

	public:
		HumanB(std::string name);
		void attack() const;
		void setWeapon(Weapon &w);	
};
#endif
