
#include <iostream>
#include <fstream>

bool initialize(int argc, char *argv[], std::string &target);
void replace_func(std::ifstream &input_file, std::string &target,
       std::string &replace_word, std::ofstream &output_file);

int main(int argc, char *argv[])
{
	std::string name_file;
	std::string generated_file("");
	std::string target("");
    std::string replace(""); 
	std::ifstream input_file;
    std::ofstream output_file;
    std::ifstream temporary_file;
    std::ofstream final_file;
        
    if(!initialize(argc, argv, target))
		return -1;

    if(argc == 4)
    {
        target = argv[2];
        replace = argv[3];
    }

	name_file = argv[1];
	input_file.open( name_file.c_str() , std::ifstream::in);
	if(input_file.is_open() == false)
	{
		std::cout<<"Problem opening input file"<<std::endl;
		input_file.close();
		return -1;
	}

	generated_file.append(name_file.substr(0, name_file.find('.')));
	generated_file.append(".replace");
	output_file.open(generated_file.c_str(), std::ios::out);
	if(output_file.is_open() == false)
	{
		std::cout<<"Problem opening output file"<<std::endl;
		input_file.close();
		output_file.close();
		return -1;
	}
	std::cout<<"generated: "<<generated_file<<::std::endl;
	replace_func(input_file, target, replace, output_file );
    
	input_file.close();
    output_file.close();
    
    system("leaks -q exercise04");
    return 0;	
}


bool initialize(int argc, char *argv[], std::string &target)
{
	
	if(argc <= 3 || argc > 4)
	{
		std::cout<<"Error en el numero de parametros introducidos"<<std::endl;
		return false;
	}else if(argc == 3)
	{
		target = argv[2];
		if(target.compare("") == 0)
		{
			std::cout<<"Error: Debes indicar una cadena valida para reemplazar"
				<<std::endl;
			return false;
		}
	}

	return true;
}

void replace_func(std::ifstream &input_file, std::string &target,
       std::string &replace_word, std::ofstream &output_file)
{   
	char c;
    char aux_char;
    int i = 0;
	std::string str1("");
	
    if(input_file.fail())
		std::cout<<"ha fallado la apertura del fichero"<<std::endl;

	std::cout<<"Estamos parseando la cadena!!!"<<std::endl;
    input_file>>std::noskipws>>c;
	str1.append(1, c);
	while(input_file.eof() == false)
	{
        if(str1[0] == target[0])
        {
            i = 0;
            while(str1[i] == target[i] && input_file.eof() == false)
            {
		        input_file>>std::noskipws>>c;
                str1.append(1, c);
                i++;
            }

            aux_char = str1[str1.length() - 1 ];
            str1 = str1.substr(0, str1.length() - 1);
            if(str1.compare(target) == 0)
            {
                str1 = "";
                output_file<<replace_word;
            }
            else
            {
                output_file<<str1[0];
                if(str1.length() > 1)
                    str1 = str1.substr(1);
                else
                    str1 = "";
            }

            str1.append(1, aux_char);
        }
        else
        {
            if(str1.length() > 1)
            {
                output_file<<str1[0];
                str1 = str1.substr(1);
            }
            else if(str1.length() == 0)
            {
                input_file>>std::noskipws>>c;
                str1.append(1, c);
            }
            else
            {
               output_file<<str1[0];
               str1 = "";
               input_file>>std::noskipws>>c;
               str1.append(1,c);
            }     
        }
	}
}

