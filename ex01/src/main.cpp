
#include <iostream>
#include "../inc/Zombie.hpp"
#include <stdlib.h>

void check_leaks(void)
{
	system("leaks -q brainz");
}

int main(void)
{
    const int n = 4;
	Zombie *zombie_collection;
    zombie_collection = zombieHorde(n, "Juanan");
    for(size_t i = 0; i < n; i++)
    {
	    zombie_collection[i].announce();
    }

    atexit(check_leaks);
    delete[] zombie_collection;
	return 0;
}
