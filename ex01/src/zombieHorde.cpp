#include "../inc/Zombie.hpp"

Zombie* zombieHorde(int N, std::string name)
{
    if(N < 0)
    {
        std::cout<<"We cannot create a negative number of zombies"
            <<std::endl;
        return NULL;
    }
    Zombie *arr_zombie = new Zombie[N];
    
    for(size_t i = 0; (int)i < N; i++)
    {
        arr_zombie[i] = Zombie(name);
    }
 return arr_zombie;   
}
