
#ifndef ZOMBIE_HPP_
#define ZOMBIE_HPP_

#include <string>
#include <iostream>

class Zombie
{
	private:
		std::string _name;
	public:
		/*Constructors and destructors*/
		Zombie(std::string name);
        Zombie();
		~Zombie();
		//-----------------------------

		void announce(void);

};

Zombie* zombieHorde(int N, std::string name);

#endif
