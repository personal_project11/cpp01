#include <iostream>
#include <string>


int main(void)
{
	std::string str = "HI THIS IS BRAIN";
	std::string *stringPTR = &str;
	std::string &stringREF = str;
	
	std::cout<<"Direccion de memoria de la variable: "
		<<&str<<std::endl;
	std::cout<<"Direccion de memoria contenida en stringPTR: "
		<<stringPTR<<std::endl;
	std::cout<<"Direccion de memoria contenida en stringREF: "
		<<&stringREF<<std::endl;
	std::cout<<"-------------------------------"<<std::endl;
	std::cout<<"string: "<<str<<std::endl;
	std::cout<<"stringPTR: "<<*stringPTR<<std::endl;
	std::cout<<"stringREF: "<<stringREF<<std::endl;
		
	system("leaks -q this_is_brain");	
	return 0;
}
