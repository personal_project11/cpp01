
#ifndef ZOMBIE_HPP_
#define ZOMBIE_HPP_

#include <string>
#include <iostream>

class Zombie
{
	private:
		std::string _name;

	public:
		/*Constructors and destructors*/
		Zombie(std::string name);
		~Zombie();
		//-----------------------------

		void announce(void);

};

void randomChump(std::string name);
Zombie* newZombie(std::string name);

#endif
